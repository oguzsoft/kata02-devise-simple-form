# How to adding devise in my Rails project?
  * Create a Rails Project. 

  * #### Open your Gemfile file and add gem;
    ```gem 'devise' ```

  * #### Run bundle command
    ```bundle install```

  * #### Install devise
    ```rails g devise:install```

  * ### Configure Devise
   Open up ```config/environments/development.rb``` and add this line
   ```config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }```
   before the ```end``` keyword.

  * ### Open up ```app/views/layouts/application.html.erb``` and add:

    ```
    <% if notice %>
      <p class="alert alert-success"><%= notice %></p>
    <% end %>
    <% if alert %>
      <p class="alert alert-danger"><%= alert %></p>
    <% end %>
    ```
  right above
  ```<%= yield %>```

### Setup the Devise Model
 ```
 rails g devise user
 rails db:migrate
 ```

 * #### Create your first user
  * Restart the server
  * And open browser and go to http://localhost:3000/users/sign_up

 * #### Add sign-up and login links
  * All we need to do now is to add appropriate links or notice about the user being logged in in the top right corner of the navigation bar.
    
  * Open ```app/views/layouts/application.html.erb``` and add:

    ```
      <p class="navbar-text pull-right">
        <% if user_signed_in? %>
          Logged in as <strong><%= current_user.email %></strong>.
          <%= link_to 'Edit profile', edit_user_registration_path, :class => 'navbar-link' %> |
          <%= link_to "Logout", destroy_user_session_path, method: :delete, :class => 'navbar-link'  %>
        <% else %>
          <%= link_to "Sign up", new_user_registration_path, :class => 'navbar-link'  %> |
          <%= link_to "Login", new_user_session_path, :class => 'navbar-link'  %>
        <% end %>
        </p>
    ```
# Finally
  * Finally, force the user to redirect to the login page if the user was not logged in. Open up ```app/controllers/application_controller.rb``` and add:
    ```before_action :authenticate_user!```

  * # Adding simple_form
    * Open your Gemfile file and add:
      ```gem simple_form```
    * Run bundle command on your console
      ```bundle install```
    * Install simple_form
      ```rails g simple_form:install```

    * ### Using simple_form
      * Change your ```app/views/people/_form.html.erb```

        ```
          <%= simple_form_for(@person) do |form| %>
          <% if person.errors.any? %>
            <div id="error_explanation">
              <h2><%= pluralize(person.errors.count, "error") %> prohibited this person from being saved:</h2>
              <ul>
              <% person.errors.full_messages.each do |message| %>
                <li><%= message %></li>
              <% end %>
              </ul>
            </div>
            <% end %>
            <%= form.input :name %>
            <%= form.input :lastname %>
            <%= form.input :phone %>
            <%= form.submit %>
          <% end %>
        ``` 

