class ApplicationController < ActionController::Base
  #Kullanıcı giriş yapmalı.
  #Kullanıcı giriş yapmaz ise direkt olarak giriş ekranına yönlendirilecektir.
  before_action :authenticate_user!
end
